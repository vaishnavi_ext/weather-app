import "react-native";
import { cleanup, fireEvent, render } from "@testing-library/react-native";
import Main from "../Components/Screens/Main";
import { Platform } from "react-native";

jest.doMock("react-native/Libraries/Utilities/Platform.android.js", () => ({
  OS: "android",
  select: jest.fn(),
}));

jest.doMock("react-native/Libraries/Utilities/Platform.ios.js", () => ({
  OS: "android",
  select: jest.fn(),
}));

jest.mock("react-native-permissions", () => ({
  PERMISSIONS: {
    ANDROID: "ACCESS_FINE_LOCATION",
    IOS: "LOCATION_WHEN_IN_USE",
  },
  RESULTS: {
    GRANTED: "granted",
  },
  request: jest.fn().mockImplementationOnce(() => Promise.resolve("granted")),
}));

jest.mock("@react-native-community/geolocation", () => {
  return {
    getCurrentPosition: jest.fn().mockImplementation((position) => {
      position({
        message: "latitude and longitude",
      });
    }),
  };
});

describe("Testing the main component", () => {
  afterEach(cleanup);
  it("Rendering the component", () => {
    render(<Main />);
  });
  it("Api mocking", async () => {
    const mockResponse = {
      data: [
        {
          location: {
            name: "hyderabad",
            country: "India",
          },
          current: {
            temp_c: 28,

            condition: {
              text: "Mist",
            },

            humidity: 79,
            cloud: 75,
          },
        },
      ],
    };
    global.fetch = jest.fn().mockResolvedValueOnce({
      json: jest.fn().mockResolvedValueOnce(mockResponse),
    });
    const { getByTestId } = render(<Main />);
    const btnFetch = getByTestId("btn");
    fireEvent.press(btnFetch);
  });

  it("Should be android", () => {
    Platform.OS = "android";
    expect(Platform.OS).toEqual("android");
  });

  it("android second condition", () => {
    Platform.OS = "android";
    expect(Platform.OS).not.toEqual("ios");
  });

  it("ios second condition", () => {
    Platform.OS = "ios";
    expect(Platform.OS).not.toEqual("android");
  });

  it("Should be ios", () => {
    Platform.OS = "ios";
    expect(Platform.OS).toEqual("ios");
  });

  it("Input field", () => {
    const { getByTestId } = render(<Main />);
    const searchText = getByTestId("input-search");
    fireEvent.changeText(searchText, "hyderabad");
    expect(searchText.props.value).toBe("hyderabad");
    const btn = getByTestId("btn");
    fireEvent.press(btn);
  });
});
