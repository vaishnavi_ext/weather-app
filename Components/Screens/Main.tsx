import {
  View,
  Text,
  SafeAreaView,
  ImageBackground,
  TextInput,
  Platform,
  Dimensions,
  Image,
  Alert,
  TouchableOpacity,
} from "react-native";
import React, { useEffect, useState } from "react";
import { eveningSun, location, night, rainy, snow, sunny } from "../Assets";
import { colors } from "../Utils/theme";

import { PERMISSIONS, request } from "react-native-permissions";
import Geolocation from "@react-native-community/geolocation";
import { styles } from "./styles";
interface Region {
  latitude: number;
  longitude: number;
}
const Main = () => {
  const weatherBackgrounds: any = {
    "Partly cloudy": rainy,
    "Light rain": rainy,
    Mist: eveningSun,
    Overcast: snow,
    Clear: sunny,
  };
  const [region, setRegion] = useState<Region>({
    latitude: 0,
    longitude: 0,
  });
  const [apiData, setApiData]: any[] = useState([]);
  const [text, setText] = useState("hyderabad");
  const permissionSettings = async (permissions: any) => {
    const status = await request(permissions);
    if (status === "granted") {
      console.log("You can use the location");
      getCoords();
    } else {
      Alert.alert("Permissions denied");
    }
  };
  useEffect(() => {
    if (Platform.OS === "android") {
      permissionSettings(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);
    } else {
      permissionSettings(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE);
    }
  }, []);

  const getCoords = () => {
    Geolocation.getCurrentPosition((position) => {
      setRegion({
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
      });
    });
  };

  const apiFetch = async () => {
    try {
      let url = `https://api.weatherapi.com/v1/current.json?key=d3775cfffbbe415c86095938233108&q=${text}`;
      const response = await fetch(url, {
        method: "get",
      });
      const data = await response.json();
      setApiData(data);
      setText("");
    } catch (error) {
      console.log("Error fetching data:", error);
    }
  };

  const onChangeTextPlace = (itemText: any) => {
    setText(itemText);
  };
  return (
    <SafeAreaView>
      <ImageBackground
        source={weatherBackgrounds[apiData.current?.condition?.text] || sunny}
        style={styles.bg}
        resizeMode="cover"
      >
        {apiData && (
          <View>
            <View style={styles.view1}>
              <View style={styles.view2}>
                <Text style={styles.temp}>{apiData.current?.temp_c}&deg;C</Text>
              </View>
              <View style={styles.view3}>
                <Text style={styles.hum}>
                  H: {apiData.current?.humidity}&deg;
                </Text>
                <Text style={styles.cloud}>
                  L: {apiData.current?.cloud}&deg;
                </Text>
              </View>
            </View>
            <View style={styles.view4}>
              <Image source={location} style={styles.location} />
              <Text style={styles.locationTxt}>
                {apiData.location?.name}, {apiData.location?.country}
              </Text>
            </View>
          </View>
        )}
        <View style={styles.view5}>
          <TextInput
            testID="input-search"
            placeholder="Enter the city name"
            placeholderTextColor={colors.black}
            onChangeText={(t) => onChangeTextPlace(t)}
            value={text}
            style={styles.txtInput}
          />
          <TouchableOpacity
            testID="btn"
            onPress={() => apiFetch()}
            style={styles.btn}
          >
            <Text style={styles.btnTxt}>Check your weather!</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </SafeAreaView>
  );
};

export default Main;
