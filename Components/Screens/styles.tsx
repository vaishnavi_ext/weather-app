import { Dimensions, Platform, StyleSheet } from "react-native";
import {
  responsiveHeight,
  responsiveWidth,
} from "react-native-responsive-dimensions";
import { colors } from "../Utils/theme";

export const styles = StyleSheet.create({
  bg: {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
  },
  view1: {
    flexDirection: "row",
    justifyContent: "flex-end",
    marginTop: responsiveHeight(10),
  },
  view2: { justifyContent: "center", marginHorizontal: 15 },
  temp: {
    fontFamily: "Nunito-SemiBold",
    fontSize: 45,
    color: colors.white,
  },
  view3: { marginRight: 20, marginVertical: 10 },
  hum: {
    fontFamily: "Nunito-Regular",
    fontSize: 20,
    color: colors.white,
    marginBottom: 5,
  },
  cloud: {
    fontFamily: "Nunito-Regular",
    fontSize: 20,
    color: colors.white,
  },
  view4: {
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center",
  },
  location: {
    width: responsiveWidth(5),
    height: responsiveHeight(3),
    marginHorizontal: Platform.OS === "android" ? 10 : 5,
  },
  locationTxt: {
    fontFamily: "Nunito-Regular",
    fontSize: 15,
    marginRight: Platform.OS === "android" ? 20 : 10,
    color: colors.white,
  },
  view5: {
    position: "relative",
    top: 350,
  },
  txtInput: {
    backgroundColor: colors.gray,
    paddingHorizontal: 10,
    borderRadius: 10,
    fontFamily: "Nunito-Regular",
    width: responsiveWidth(70),
    height: Platform.OS === "ios" ? responsiveHeight(7) : responsiveHeight(7),
    marginHorizontal: responsiveWidth(15),
  },
  btn: {
    borderColor: colors.gray,
    width: responsiveWidth(40),
    height: responsiveHeight(5),
    borderWidth: 1,
    marginHorizontal: responsiveWidth(30),
    marginVertical: responsiveHeight(5),
    justifyContent: "center",
    borderRadius: 5,
  },
  btnTxt: {
    textAlign: "center",
    fontFamily: "Nunito-Regular",
    fontSize: 15,
  },
});
