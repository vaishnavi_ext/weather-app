export const colors = {
  background: '#F4F4F4',
  black: '#2E2D2D',
  gray: '#E0E0E0',
  darkGray: '#A1A1A1',
  purple: '#9682B6',
  white: '#fff',
  darkPurple: '#4B194F',
  green: '#1ECA2F',
  yellow: '#F5DF55',
};
