import {View, Text} from 'react-native';
import React from 'react';
import Main from './Components/Screens/Main';

const App = () => {
  return <Main />;
};

export default App;
